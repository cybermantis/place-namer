from random import choice

def readFile(fn):
    output = []
    with open(fn, "r") as f:
        for line in f:
            line = line.strip()
            if line not in output:
                output.append(line)
    return output

terrain = readFile("terrain.txt")
adjectives = readFile("adjectives.txt")

def randomName():
    name = choice(adjectives) + " " + choice(terrain)
    return name.title()

if __name__ == "__main__":
    print(randomName())
    from os import system
    system("pause")
